#!/bin/bash
set -e
# APPSFILE=docs/apps.rst
# if type getlino &> /dev/null ; then
#   echo "Updating $APPSFILE"
#   getlino list --rst > $APPSFILE
# else
#   echo "Not updating $APPSFILE because getlino is not installed."
# fi
BOOK=../book/docs
if [ -d $BOOK ] ; then
  cp -au $BOOK/shared docs/
  cp -au $BOOK/copyright.rst docs/
fi
# python make_docs.py
