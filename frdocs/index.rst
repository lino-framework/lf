.. _lf:

==================
Lino le framework
==================

Ce site web expliquera en français ce que c'est ce fameux *Lino framework*.

Pour l'instant il vaut mieux aller voir `l'original en anglais
<https://www.lino-framework.org/>`__.
