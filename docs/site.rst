.. _cg.lino.site:

===========
Screenshots
===========

A :term:`Lino site` is a website that focuses on structured data. Users can
enter, modify, visualize and otherwise use that data according to the business
rules and their permissions.

Some screenshots:

.. figure:: 07.png
  :width: 90%

  A :term:`main page` after signing in as user Robin in :ref:`cosi`.  The only
  :term:`dashboard` item is an overview of your :term:`ledger journals
  <journal>`.

.. figure:: 09.png
  :width: 90%

  A :term:`main page` after signing in as user Robin in :ref:`amici` using the
  :term:`React front end`. The :term:`dashboard` contains a series of items,
  the first being the "My appointments" table.

.. figure:: 02.png
  :width: 90%

  A list of your :term:`sales invoices <sales invoice>`.


.. figure:: 03.png
  :width: 90%

  A :term:`detail window` of a sales invoice.

.. figure:: 04.png
  :width: 90%

  A :term:`sales invoice` as a :term:`printable document`.

.. figure:: 05.png
  :width: 90%

  The :term:`detail window` of an :term:`organization` in :ref:`cosi`.

.. figure:: 08.png
  :width: 90%

  A :term:`detail window` of a :term:`person` in :ref:`cosi`.

.. figure:: 06.png
  :width: 90%

  The :menuselection:`Configure` menu in :ref:`cosi`.

.. figure:: 10.png
  :width: 90%

  The :term:`detail window` of a :term:`person` in :ref:`amici`.

.. figure:: 11.png
  :width: 90%

  The :term:`detail window` of an :term:`organization` in :ref:`amici`.

.. figure:: 12.png
  :width: 90%

  The Family tab in the :term:`detail window` of a :term:`person` in
  :ref:`amici`.

.. figure:: 13.png
  :width: 90%

  Weekly calendar view in :ref:`amici`.

.. figure:: 14.png
  :width: 90%

  Enrolments tab in the :term:`detail window` of an :term:`activity` in
  :ref:`voga`.
