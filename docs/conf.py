# -*- coding: utf-8 -*-
# html_theme = "sphinx_rtd_theme"
# sphinx_rtd_theme doesn't show the languages selector sidebar


from atelier.sphinxconf import configure; configure(globals())
from lino.sphinxcontrib import configure; configure(globals())

# General substitutions.
project = "Lino framework"
import datetime

copyright = '2019-{} Rumma & Ko Ltd'.format(datetime.date.today().year)
html_title = "Lino "
htmlhelp_basename = 'lf'
extensions += ['lino.sphinxcontrib.logo']

html_context.update({
    'display_gitlab': True,
    'gitlab_user': 'lino-framework',
    'gitlab_repo': 'lf',
})

# html_theme_options.update(globaltoc_includehidden=False)

inheritance_graph_attrs = dict(rankdir="TB")
# inheritance_graph_attrs.update(size='"12.0, 16.0"')
inheritance_graph_attrs.update(size='"48.0, 64.0"')
inheritance_graph_attrs.update(fontsize=14, ratio='compress')

if False:
    # extensions += ['yasfb']
    # extensions += ['sphinxcontrib.feed']
    extensions += ['sphinxfeed']
    # NB : not the public sphinxfeed but my extended version
    feed_base_url = 'https://community.lino-framework.org'
    feed_author = 'Luc Saffre'
    feed_title = "Lino Community Guide"
    feed_field_name = 'date'
    feed_description = "News about the Lino Community"

# extensions += ['sphinx_simplepdf']  # if-builder directive

# extensions += ['hieroglyph']  # Generate HTML presentations
# autoslides = False
# slide_numbers = True

import os

os.environ['LC_TIME'] = 'de_BE.UTF-8'

# language = 'en'
# locale_dirs = ['locales/']
# gettext_compact = False
# translated_languages = ['de', 'fr']  # used by atelier

from pprint import pprint
# print("20230616", templates_path, html_static_path)
