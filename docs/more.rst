.. _lino.why:
.. _lf.more:

===============
More about Lino
===============

There's more to say about Lino.

.. toctree::
   :maxdepth: 1

   big
   values
   name
   testimonals
   copyright
   who
