.. _lf.contact:

=======
Contact
=======

Content moved to :doc:`/copyright`.

..
  How to contact the Lino community:

  - For technical help send an email to ``team`` at ``lino-framework.org``.
    or report your issues in the `book repository
    <https://gitlab.com/lino-framework/book>`__ on GitLab.

  - For any other questions send an email to ``info`` at ``lino-framework.org``.

  - Or directly contact a member of the `Lino team at Rumma & Ko
    <https://www.saffre-rumma.net/team/>`__

  - Or contact any of the organizations and individuals who use Lino or contribute
    to it.  See

.. For non-technical and commercial information about Lino
   please contact :doc:`rumma`.

.. Subscribe to one of our mailing lists:
   `news <https://lino-framework.org/cgi-bin/mailman/listinfo/news>`__
   or
   `lino-developers <https://lino-framework.org/cgi-bin/mailman/listinfo/lino-developers>`__.

.. - We are on `lino-core:matrix.org <https://matrix.to/#/#lino-core:matrix.org?via=matrix.org>`__
