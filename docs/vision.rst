===============
The Lino vision
===============

Content moved to https://www.synodalsoft.net/vision/


Public Money? Public Code!
==========================

The Lino vision can be seen as a proof of concept for the `Public Money? Public
Code! <https://fsfe.org/activities/publiccode/publiccode.de.html>`__ campaign
led by the `Free Software Foundation Europe
<https://fsfe.org/about/index.html>`__.


.. image:: lino-pmpc.jpg

Above image was generated with the `Sharepic Generator
<https://sharepic.fsfe.org/#pmpc>`__
