Welcome to the **main website** about the Lino framework.

Lino is a framework for developing customized database applications `based on Django <https://dev.lino-framework.org/about>`__.
Lino is maintained by the `Synodalsoft <https://www.synodalsoft.net/>`__ team,
a project led by `Luc Saffre <https://luc.lino-framework.org/about>`_ at `Rumma & Ko Ltd <https://www.saffre-rumma.net/>`__.
Lino is `sustainably free software <https://www.synodalsoft.net/free/#sustainably-free-software>`__.

This website is meant for everybody.
Software developers might want to jump to the `Developer Guide <https://dev.lino-framework.org/>`__,
system administrators to the `Hosting Guide <https://hosting.lino-framework.org/>`__.



.. _lf:

==================
The Lino framework
==================

..
  .. sidebar::

  .. image:: ../docs/wanted.png
    :width: 100%

..
  **Lino** is a software framework for building generic customized database
  applications based on Django. It is being used since 2010 on a handful of sites
  in Belgium.  Developed and maintained by a family-sized international team, it
  is placidly on its way to become a standard solution for sustainably free
  application development :doc:`(more) <framework>`.


.. toctree::
   :maxdepth: 1

   see
   framework
   library
   get
   money
   more


.. toctree::
   :hidden:

   whichapp
   contact
   vision
   free
   dream
