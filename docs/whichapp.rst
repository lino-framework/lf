.. include:: /shared/include/freesvg.rst

=================================
Which Lino application is for me?
=================================

First answer: don't worry. We'll find out together.


.. list-table:: Lino plugins and applications

  -
    *
    * `Così <https://cosi.lino-framework.org/>`__
    * `Voga <https://voga.lino-framework.org/>`__
    * `Tera <https://tera.lino-framework.org/>`__
    * `Welfare <https://welfare.lino-framework.org/>`__
    * `Noi <http://noi.lino-framework.org/>`__
    * `Amici <https://amici.lino-framework.org/>`__

  -
    * :ref:`contacts <ug.plugins.contacts>`
    * yes
    * yes
    * yes
    * yes
    * yes
    * yes

  -
    * :ref:`accounting <ug.plugins.accounting>`
    * yes
    * yes
    * yes
    * no
    * yes
    * no

  -
    * :ref:`calendar <ug.plugins.cal>`
    * no
    * yes
    * yes
    * yes
    * yes
    * yes

  -
    * :ref:`activities <ug.plugins.courses>`
    * no
    * yes
    * yes
    * no
    * yes
    * yes




..  .. rubric:: Lino Così -- 40 €\ :superscript:`*`

.. grid:: 1 2 3 3

 .. grid-item::

  |invoicing| |accounting|

  .. rubric:: Lino Così -- That's how we love accounting

  .. button-link:: https://cosi.lino-framework.org/
    :expand:

    Offers | Products | Invoicing | Accounting | VAT declaration

 .. grid-item::

  |calendar| |invoicing| |accounting|

  .. rubric:: Lino Voga -- Lino for training centres

  .. button-link:: https://voga.lino-framework.org/
    :expand:

    Activities | Calendar | Enrolments | Invoicing | Accounting

 .. grid-item::

  |calendar| |invoicing| |accounting|

  .. rubric:: Lino Tera -- Lino for therapy centres

  .. button-link:: /lino/tera
    :expand:

    Clients | Calendar | Invoicing | Accounting

 .. grid-item::

  |calendar| |reception| |social| |handshake|

  .. rubric:: Lino Welfare -- |br| Lino for Social Welfare Centres

  .. button-link:: /lino/welfare
    :expand:

    Clients | Calendar | Reception | Contracts | Aids | Courses

 .. grid-item::

  |working|

  .. rubric:: Lino Noi -- Lino for teams

  .. button-link:: /lino/noi
    :expand:

    Project management | Combined issue / work time tracking

 .. grid-item::

  |family| |calendar|

  .. rubric:: Lino Amici -- Lino for families

  .. button-link:: /lino/amici
    :expand:

    Contacts | Calendar
