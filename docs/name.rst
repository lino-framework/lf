.. _lino.name:

About the name "Lino"
=====================

Lino is a common Italian first name, whose most famous bearer was probably `Lino
Ventura <https://en.wikipedia.org/wiki/Lino_Ventura>`_.

Yes, we wanted **a human name** for our framework. Lino makes it possible to
write complex applications that are like children: they grow day by day, giving
parental joy to those who collaborate to their development.

"Lino" is also an acronym of the words **Linked objects**.  Read more about this
in :doc:`values`.

Note that Lino the framework has nothing to do with the `German company of same
name <https://www.lino.de/>`_.  We don't feel disturbed by them and hope that they
don't do so either.
