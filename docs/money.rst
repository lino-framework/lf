.. _lf.money:

===========================
How to make money with Lino
===========================

Lino is :term:`Free Software`, where "free" is meant as in free *speech*, not as
in free *beer*. So here is how you can make money with Lino, either on your own
or together with others.

As an **independent developer** you can write and then maintain your own
:term:`Lino application` for a customer. You are a :term:`development provider`
and your customer(s) pay(s) you for that service.

As an **inhouse developer** you write a Lino application for your employer.
Simply replace "customer" by "employer" in the previous paragraph.

As a **hosting provider** you may install and maintain Lino :term:`production
sites <production site>` for your customers. This art is documented in
:ref:`hg`.

As a **SaaS provider** you develop, host and maintain a :term:`Lino application`
for yourself and ask money from it users. The AGPL encourages you to think
synodal by obliging you to publish your application's source code.

As a **core developer** paid by Synodalsoft (Rumma & Ko Ltd) you help us with
improving Lino as a framework. See :ref:`ss.jobs`.
