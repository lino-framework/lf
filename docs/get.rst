.. _lf.get:

=================
How to get a Lino
=================

We recommend two possible approaches for getting your own :term:`Lino site`:

- pick a third-party application developer from our :ref:`directory of Lino
  service providers <cg.who>`

- ask your existing IT team or external :term:`application developer` to
  install a Lino for you as explained in the
  `Hosting Guide <https://hosting.lino-framework.org>`__
  and the `Developer Guide <https://dev.lino-framework.org>`__.
