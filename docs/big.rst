.. _lf.big:

===========
Lino is big
===========

.. contents::
   :depth: 1
   :local:


An encompassing framework
=========================

Lino is an *encompassing* framework. It covers:

- *back-end technologies* (server, application, database)
- *front-end technologies* (JavaScript, ExtJS, React)
- *documentation technologies* (documentation for end users and developers are interconnected)
- *templating technologies* for maintaining standard business documents


Lino fundamentally influences the way of collaboration between end users and
developers.

Lino applications are being used in both the public and the private sector.

Lino is not only for writing applications, it is also a documentation framework.
It provides tools and conventions that make the developer *want* to write
documentation.  And that documentation is an integral part of Lino.

Lino uses an innovative templating approach that encourages communication
between end users and developers even for seemingly small details like a new
bank account number or a layout change.

Both the documentation and the templates can be edited by either the site
operator or the server administrator.

.. _lf.existing_apps:

A diverse collection of applications
====================================

Lino has a growing collection of actively maintained `applications <Lino
applications>`_  that can be used out of the box by a :term:`SaaS provider
<hosting provider>`, or as a base for a new :term:`Lino application` by a
:term:`development provider`.

There are two classes of applications in our collection, which differ by how
their documentation and test suites are organized.

First of all we have the :term:`privileged applications`:

- :ref:`noi` is the application we use for
  managing our collaboration.  It's about tickets, projects and working time.
- :ref:`cosi` a simple accounting application.
- :ref:`voga` is about organizing courses, registering participants, invoicing, accounting
- :ref:`tera` is about therapies, invoicing, accounting
- :ref:`avanti` is used to manage immigrants during their integration

And then a series of :term:`stand-alone applications` that are being used on
some :term:`production site`:

- :ref:`amici` is a contacts manager for families.

- :ref:`welcht` and :ref:`weleup` are used in production
  by two Belgian Public Social Welfare Centres,
  they use a common :term:`plugin library` :ref:`welfare`.

- :ref:`presto` (group calendar, team management, monthly invoicing) was
  developed for a service provider in Eupen. Accounting data is being exported
  to a third-party application. Not yet used in production.

- :ref:`pronto` is an application developed for a provider with delivery notes
  and with accounting. Not yet used in production.

- :ref:`mentori`, :ref:`shop` and *Lino Medico* are applications
  developed by somebody else than the author.

See also :ref:`getlino.apps`.
