.. _cg.lino.framework:
.. _lf.framework:

===================
Lino is a framework
===================

Lino is "a framework for developing custom database applications". What does
that mean?


.. contents::
   :depth: 1
   :local:

Vocabulary
==========

.. glossary::

  database application

    A computer program that allows its users to store  data in a
    :term:`database` in order to share it with other users.

  software framework

    A set of software tools used by developers who write and maintain
    applications for their employer or their customers.


Lino is invisible to end users
==============================

A :term:`database application` is something you know:  it's a website that lets
you store and retrieve data in a database using menus, data entry forms, list
views, etc. It lets you share your data with other people and protects it from
unauthorized access.  It can give different users different access rights.
Facebook, Gmail and Twitter are popular examples of :term:`database applications
<database application>`.

But have you ever heard of Python, Java, React, Scala and Ruby? These are
*tools* used to *build* Facebook, Gmail and Twitter. You don't need to know them
unless you're a programmer.

Lino is **not directly visible** because it is used by professional
:term:`software developers <software developer>` to develop and maintain
database applications that match your specific needs.

Lino is **indirectly tangible** because it fosters a certain philosophy of
future-proof collaboration between end users and developers. We call this
philosophy `synodal software development <https://www.synodalsoft.net/ssd/>`__.

Lino features
=============

Lino comes with a range of ready-to-use :term:`applications <Lino application>`.

Running a :term:`Lino site` has a low total cost of ownership because you need
virtually no :term:`support` once you are up and running.

A :term:`Lino application` can reuse a vast library of existing functionality
and yet is highly customized to your needs.

Lino is a powerful and flexible framework that grows with your needs.
