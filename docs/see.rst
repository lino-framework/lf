.. _lino.see:

====================
What Lino looks like
====================

All Lino applications look the same. A social worker who has been using
:ref:`welfare` for years, and then changes jobs and starts as a secretary using
:ref:`cosi`, would immediately feel at home and recognise that these two
applications have the same roots.  This is remarkable when you consider how
different :ref:`welfare` and :ref:`cosi` actually are.


.. toctree::
   :maxdepth: 1

   site
   demos
