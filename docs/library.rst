.. _lf.plugins:

====================================
Lino is a library of functionalities
====================================

Lino comes with wide set of existing functionalities. They are organized into
about 100 reusable modules.  We call them "plugins", but they are more than
pluggable: they are also extensible, and they interact with each other. Here is
an introduction. For a full reference see :ref:`ug.plugins`.

.. contents::
  :local:


Lino applications
=================

..
  Lino is successfully being used since 2010 in medium-sized organizations. But
  also small organizations need flexible customizable database applications.
  Because customized software development is expensive, and because they have less
  resources, they often settle down with suboptimal mass products.

Lino applications feature a wide range of highly customizable and integrated
modules for

- contacts management
- calendar -- fully integrated into your other data
- reception and waiting room management
- enrollment management for activities, courses, events or travels
- standard office functionality -- uploads, excerpts, comments, summaries, reports
- client coaching
- households and family members
- job search assistance -- skills, career and curriculum
- invoicing -- automatically generate invoices
- accounting -- general accounting, purchases, bank statements, payment orders, VAT declarations
- issue tracker and project management
- work time tracking and service reports



Contacts
========

Lino's :ref:`contacts <ug.plugins.contacts>` fundamentally differentiates
between "persons" and "organizations". In Lino, each "person" can act as a
"contact" for multiple organizations. Some simple contacts management systems
just have a field "Company" for every "contact". That's not enough for
professional customer relationship management (CRM) where you want to know when
a same person works for your customer X, but is also a member of association Y?

This differentiation between "persons" and "organizations" is possible, but not
imposed. You might *not* want to differentiate between them and just see all
your "partners" in a single list.

The contacts in Lino can receive application-specific **specializations**.
Depending on what you do with Lino,  a "doctor", a "patient", a "member", an
"employee" might mean much more than just a person. For example

- in :ref:`tera` you have "patients",
- in :ref:`welfare` you have "clients",
- in :ref:`voga` you have "members",
- in :ref:`presto` you have "workers".

The contacts plugin usually implies the :ref:`countries <ug.plugins.countries>`
plugin, which adds countries and cities (any kind of geographical places).

If you want to record **multiple addresses** per partner, you probably want the
:ref:`addresses <ug.plugins.addresses>` plugin.  For example in :ref:`welfare`
we differentiate between the "residence" and the "contact" address of a client.
Or in :ref:`amici` you can see a history of the places where a person has been
living.

The bare contacts plugin provides a number of fields for specifying phone
number, email address, website url of each partner. If you want more
flexibility, ask for the :ref:`phones <ug.plugins.phones>` plugin where you can
configure your own types of **contact details** and record multiple phone
numbers or email addresses per partner.

Calendar
========

The :ref:`calendar plugin <ug.plugins.cal>` adds the notion of calendar entries.
A Lino application seamlessly integrates your calendar into your remaining data.
For example,

- a sales representative may need to know the balance of the
  customer they are going to meet;

- a therapist may need to review earlier meetings and discussions
  with the patient they are going to meet;

- a social worker may need to know that the driving license or
  the work permit of the client they are going to meet will expire in two
  months.

In most countries it is prohibited to store confidential data about other people
in a place you don't own. That's why you can't store such data on a cloud server
owned by Google or Microsoft.

"Really useful software unobtrusively blends into the lives of its users and
makes their lives easier without needing them to drastically change what it is
they do day to day" -- Jeremy Bourhis, `Why Google Calendar integration is just
not enough
<https://www.cronofy.com/blog/why-google-calendar-integration-is-just-not-enough>`__


Activities
==========

The :ref:`courses <ug.plugins.cal>` plugin adds the notion of "activities". An
"activity" is when a group of people meet regularly for doing something. An
activity generates recurrent calendar entries.

This plugin has a quite wide field of usages:

- in :ref:`voga` it is labelled "Activities". They organize courses, travels,
  hikes and excursions where people enrol and pay for their participation in
  diverse ways (subscriptions, membership fee, one-time payment, ...)

- in :ref:`welfare` it is labelled "Workshops". The social centre organizes all
  kind of activities, and the social workers want to know where their clients
  have been participating and what the workshop leader have to say about their
  clients.

- in :ref:`tera` it is labelled "Therapies". They have individual therapies
  that run over years. Or there are group therapies and or family therapies.

..
  Sometimes the participants meet only once.
  And sometimes there is only one participant.


Business activities
===================

The :ref:`products <ug.plugins.products>` plugin is when you have a list of
things that you care about. When you want this plugin, you probably also want
the :ref:`trades <ug.plugins.trading>` plugin, which enables you to sell, or
buy, or otherwise exchange your things.

The :ref:`orders <ug.plugins.orders>` plugin is  when a customer orders that
two of workers will mow the grass every two weeks between May and October. Or
another customer orders one social worker to visit their elderly mother every
Monday and Thursday. Or another customer moves from one place to another and
orders a lorry and two workers for transport.

You will then have what Lino calls :term:`trade vouchers <trade voucher>`:
invoices, delivery notes and such documents. An invoice and a delivery note are
technically very similar: they have a date, a recipient, and a list of rows,
each row pointing to a product and saying a quantity.

You can use the :ref:`trades <ug.plugins.trading>` plugin without any accounting
functionality. For example you might simply want to issue sales invoices, and at
the end of the month send a CSV or XML file to your accountant. Or your Lino
might not talk about money at all: a local exchange trade system, or a community
site for sharing cars.

An addition to the trades plugin is the :ref:`invoicing <ug.plugins.invoicing>`
plugin, which you want when you happen to automatically generate lots of
invoices in the end of the month.  Lino can generate invoices from delivery
notes, from calendar entries, from internal working sessions... actually every
database object can become an invoice generator.

Basic accounting
================

The :ref:`accounting <ug.plugins.accounting>` plugin adds basic notions for
accounting: accounts, journals, vouchers and movements.  A **voucher**, in Lino,
is any document that gets registered in a **journal** with a date and a number.

Some vouchers (for example a sales invoice) are produced by Lino, and Lino can
print them for you. Other vouchers (for example a purchase invoice) have been
issued by a partner and you just register them in a journal.

Lino knows different types of vouchers. For example **bank statements**, cash
reports and payment orders are defined in the :ref:`finan <ug.plugins.finan>`
plugin. If you want automatic communication with your bank, then you also want
the :ref:`sepa <ug.plugins.sepa>` plugin.

Invoicing versus accounting
===========================

There are people who write sales invoices with Lino but don't care about whether
they get paid (actually they just don't need Lino to care for it, for example
because they have an external service provider).

Or the opposite: people don't need Lino to write invoices (for example because
they write them by hand) but want to track their financial operations with Lino.

But if you do both with Lino, then you get extra advantages because everything
is integrated.  Lino can then tell you the customer's balance at any moment.


More accounting
===============

Your **VAT declarations** are vouchers. Defined in the :ref:`vat
<ug.plugins.vat>` plugin. Lino currently has VAT drivers for Belgium and
Estonia. We are looking forward to our first accountant sites in other
countries.

If you fill yourself the **pay rolls for your workers**,  then you want the
:ref:`wages <ug.plugins.wages>` plugin. In Belgium this task is usually done by
specialized service providers.

If you want **balance sheets** and other yearly accounting reports, then you
want the :ref:`sheets <ug.plugins.sheets>` plugin.

Households and human links
==========================

:ref:`households <ug.plugins.households>`
:ref:`humanlinks <ug.plugins.humanlinks>`

Common functionalities
======================

File uploads
------------

Many applications need a way for their users to upload arbitrary files.

That's the :ref:`uploads <ug.plugins.uploads>` plugin.

Comments
--------

Many applications need a way for their users to discuss in various ways.

The :ref:`comments <ug.plugins.comments>` plugin turns your Lino into a kind of
social media. Every database object can become topic of a discussion thread. For
example when a social worker spoke to a client, they can write a comment into
Lino, and when the social worker or one of their colleagues opens the client's
detail, they will see this comment. At least if they have permissions to see it.
Because Lino differentiates comments that are "private" or confidential from
those that aren't.

Users can reply to other people's comments or just "like" them, or show all
kinds of emotions (the list of which you can configure yourself).

Most but not all of those who use the :ref:`comments <ug.plugins.comments>`
plugin also use the :ref:`notify <ug.plugins.notify>` plugin, which is
responsible for sending notifications via websocket or email to the concerned
users. For example when our social worker writes a comment about a client, they
might want their team mate to get notified about it.


Pictures and media files
------------------------

Content management
------------------

:ref:`publisher <ug.plugins.publisher>`
:ref:`blogs <ug.plugins.blogs>`
:ref:`albums <ug.plugins.albums>`
:ref:`sources <ug.plugins.sources>`


Teams, topics and partner lists
-------------------------------

The :ref:`groups <ug.plugins.groups>` plugin adds the notions of :term:`user
groups <user group>` and :term:`user memberships <user membership>`.

The :ref:`lists <ug.plugins.lists>` plugin adds functionality for managing
partner lists.

The :ref:`topics <ug.plugins.topics>` plugin adds the notion of "topics" and
lets you  record "interest" of a :term:`partner` in a topic.

These three plugins are technically quite similar, but have different focus and
can be combined.


Data integrity
--------------

See :ref:`checkdata <ug.plugins.checkdata>`

Printable documents
-------------------

In some cases your users just want to click on a print button and get a pdf file
they can print out.  That's the :ref:`printing <ug.plugins.printing>` plugin.

It can get more complex. The site operator may want a history of every printed
document, with the worker who issued it, the recipients who received it and how
they received it. They may want a way to prevent you from inadvertently changing
any data that has been certified by that document. A frequent use case are
invoices. In this case you want the :ref:`excerpts <ug.plugins.excerpts>`
plugin.


More
====
