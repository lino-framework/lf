.. _public_demos:
.. _online_demos:
.. _demos:

=================
Online demo sites
=================

This page lists the currently available :term:`online demo sites <online demo site>`.

- `cosi1e <https://cosi1e.lino-framework.org>`__ : write invoices, maintain a
  catalog of products (see also :ref:`ug.cosi.tour`).

- `noi1r <https://noi1r.lino-framework.org>`__ : write tickets, discuss about
  them, register your working time (see also :ref:`ug.noi.tour`).

- `voga1r <https://voga1r.lino-framework.org>`__ : manage activities and their
  participants and a calendar of events, let Lino create invoices to
  participants (:ref:`voga`). Try also `voga1e
  <https://voga1e.lino-framework.org>`__, which shows the same data using the
  legacy :term:`ExtJS front end`.

- `cms1 <https://cms1.mylino.net/>`__ : write web pages, blog entries,
  newsletters, manage photos (:ref:`cms`)

- `weleup1 <https://weleup1.mylino.net/>`__  and `welcht1
  <https://welcht1.mylino.net/>`__ : manage the clients and their projects in a
  public center for social action in Belgium. (:ref:`welfare`)

.. Screen casts :

  - `Lino Così, das kleine Buchhaltungsprogramm <https://youtu.be/yT3FEuCEFWU>`__


.. _belref: http://belref.lino-framework.org
.. _demo1: http://demo1.lino-framework.org
.. _demo3: http://demo3.lino-framework.org
.. _welfare-demo: https://welfare-demo.lino-framework.org
.. _welfare-demo-fr: https://welfare-demo-fr.lino-framework.org
.. _logos-demo: http://logos-demo.lino-framework.org
.. _polly-demo: http://polly-demo.lino-framework.org
.. _patrols-demo: http://patrols-demo.lino-framework.org
.. _cosi-demo: http://cosi-demo.lino-framework.org
.. _roger: http://roger.lino-framework.org
.. _ylle: http://ylle.lino-framework.org
.. _vtp2014: http://vtp2014.lino-framework.org
.. _team: http://team.lino-framework.org/



.. removed:

  The remaining demos are rather old.

  Of general interest:

  - demo1_ shows the "Polls" application developed in the
    :ref:`lino.tutorial.polls` tutorial. A minimal didactic Lino
    application without authentication.
  - cosi-demo_ :    Official online demo of :ref:`cosi`.
  - roger_ :  Official online demo of :ref:`voga` à la Roger
  - team_ : Official online demo of :ref:`noi`.

  Specific to Belgium:

  - welfare-demo_ : online demo of :ref:`welfare` à la Eupen
  - welfare-demo-fr_ : online demo of :ref:`welfare` à la Châtelet

  Specific to Estonia:

  - ylle_       :   Online demo of :ref:`cosi` in Estonian
  - vtp2014_    :   Online demo of :ref:`voga` in Estonian

  Experimental:

  - belref_ : Official online demo of :ref:`belref`.
  - patrols-demo_ : Official online demo of :ref:`patrols`.
  - logos-demo_ : Official online demo of :ref:`logos` (as an early
    prototype for the SacredPy project)
  - polly-demo_ : Official online demo of :ref:`polly`

  - demo3_ : Official online demo of :ref:`presto`.
