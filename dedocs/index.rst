.. _lf:

==================
Das Lino-Framework
==================

.. sidebar::

  .. image:: ../docs/wanted.png
    :width: 100%


Diese Webseite erklärt, was das :term:`Lino-Framework` ist und weshalb Sie es
möglicherweise benutzen wollen.

Wir bitten um Ihr Verständnis dafür, dass die deutsche Übersetzung hinter dem
`englischen Original <https://www.lino-framework.org/>`__ her hinkt.



Neben dieser Webseite gibt es auch
die `Kooperationsrichtlinien <https://community.lino-framework.org>`__,
das `Benutzerhandbuch <https://using.lino-framework.org>`__,
den `Hosting Guide <https://hosting.lino-framework.org>`__,
den `Developer Guide <https://dev.lino-framework.org>`__
und `Luc's developer blog <https://luc.lino-framework.org>`__.


.. toctree::
   :maxdepth: 1

   framework
