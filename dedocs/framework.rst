.. _cg.lino.framework:
.. _lf.framework:

===========================
Was ist das Lino-Framework?
===========================

**Lino** ist ein :term:`nachhaltig freies <Nachhaltig Freie Software>` :term:`Framework` zum Entwickeln und Warten
von :term:`maßgeschneiderten Datenbankanwendungen <maßgeschneiderte Datenbankanwendung>`.


.. glossary::

  Lino-Framework

    Eine Sammlung von Programmen und Dokumentation, die unter der
    Internet-Domain `lino-framework.org` veröffentlicht sind.

  Lino-Anwendung

    Eine :term:`Datenbankanwendung`, die unter Verwendung des
    :term:`Lino-Frameworks <Lino-Framework>` entwickelt wurde,
    die eine bestimmte Menge von Funktionalitäten hat,
    und die durch ihre :term:`Endbenutzer` als solche erkannt wird.

    Jede :term:`Lino-Anwendung` ist ein eigenes :term:`Softwareprodukt`, das
    durch seinen jeweiligen :term:`Softwareträger` entwickelt und gepflegt wird.

  Lino-Anlage

    Die Instanz einer bestimmten :term:`Lino-Anwendung` auf einem bestimmten
    Server.

  Datenbankanwendung

    Ein Computerprogramm, das von Menschen benutzt wird, um Informationen in
    einer Datenbank zu erfassen, zu pflegen und zu nutzen.

  maßgeschneiderte Datenbankanwendung

    Eine :term:`Datenbankanwendung`, die ein :term:`Softwareträger` entsprechend
    seinen spezifischen Bedürfnissen entwickelt hat.

  Framework

    Eine Sammlung von Werkzeugen, die von mehreren Softwareentwicklern benutzt
    wird, um Anwendungen für ihre Auftraggeber zu schreiben.

    A suite of software tools used by developers who write and maintain
    applications for their employer or their customers.

  Nachhaltig Freie Software

    Software, von der alle wichtigen Komponenten unter einer freien
    Softwarelizenz zur Verfügung stehen.

  Endbenutzer

    bla bla

  Softwareprodukt

    blabla

  Softwareträger

    Die rechtliche Person, die über ein :term:`Softwareprodukt` regiert.
    Sie finanziert Pflege und Weiterentwicklung.
    Sie entscheidet souverän über alle Änderungen im Funktionsumfang.
